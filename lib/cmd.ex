defmodule Podman.Cmd do
    @moduledoc """
    Documentation for Elixir Podman bindings.
    """

    @doc """
    Hello world.

    ## Examples

        iex> DockerFactory.hello()
        :world

    """
    def hello do
        :world
    end

    def run(
            image,
            command,
            options \\ []
        ) do
        rm = case options[:rm] do
            nil -> []
            true -> ["--rm"]
        end
        tty = case options[:tty] do
            nil -> []
            true -> ["--tty"]
        end
        options = Keyword.merge(
            [into: ""],
            options
        )

        System.cmd("podman",
            ["run"]
                ++ rm
                ++ tty
                ++ [image]
                ++ command,
            into: options[:into]
        )

    end

end
